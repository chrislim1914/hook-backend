<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->options('/{any:.*}', ['middleware' => 'cors', function() {
    return response(['status' => 'success']);
  }]);

/**
 * user
 */
$router->group(['prefix' => 'api/user', 'middleware' => 'cors'], function($router)
{
    $router->post('sns', ['uses' => 'UserController@snsSignupSignin']);
    $router->post('register', ['uses' => 'UserController@registerUser']);

    $router->post('uploadphoto', ['middleware' => 'auth:users', 'uses' => 'UserController@uploadProfilePhoto']);
    $router->post('update', ['middleware' => 'auth:users', 'uses' => 'UserController@updateProfile']);


    $router->post('login', ['uses' => 'UserController@loginUser']);
    $router->post('logout', ['middleware' => 'auth:users', 'uses' => 'UserController@logoutUser']);
    $router->post('refresh', ['uses' => 'UserController@refresh']);
    $router->get('getUserData', ['middleware' => 'auth:users', 'uses' => 'UserController@getUserData']);

    // user product post
    $router->get('getPost', ['uses' => 'UserController@userPostProduct']);

    // send reset password link
    $router->post('sendEmailLink', ['uses' => 'UserController@verifyEmailUrl']);

    // send verify email link
    $router->post('resetEmailLink', ['uses' => 'UserController@forgetPasswordEmail']);
    // change the password after resetEmailLink
    $router->post('resetPassword', ['uses' => 'UserController@resetPassword']);

    // change the password
    $router->post('changePassword', ['middleware' => 'auth:users', 'uses' => 'UserController@changePassword']);


    // verify email link
    $router->get('verifyUrl/{payload}', ['uses' => 'UserController@verifyUrl']);

    // get sellet data
    $router->get('seller', ['uses' => 'UserController@getSellerData']);
    $router->post('exchange/set', ['uses' => 'UserController@setExchange']);
    $router->get('exchange', ['uses' => 'UserController@getExchange']);
});

/**
 * weather
 */
$router->group(['prefix' => 'api'], function($router)
{
    $router->get('weather', ['middleware' => 'cors', 'uses' => 'WeatherController@getCCandFC']);
});

/**
 * buy and sell
 */
$router->group(['prefix' => 'api', 'middleware' => 'cors'], function($router)
{
    // carousell
    $router->get('buyandsell', ['uses' => 'BuyAndSellController@mergeFrontDisplay']);
    $router->get('buyandsellview', ['uses' => 'BuyAndSellController@viewSingleContent']);
    $router->get('buyandsellfeed', ['uses' => 'BuyAndSellController@feedBuyandSell']);
    $router->get('buyandsellfilter', ['uses' => 'BuyAndSellController@buyAndSellFilterForPage']);
    $router->get('carousellcategory', ['uses' => 'CarousellController@loadCarousellCategory']);

    // our own
    $router->post('product/add', ['middleware' => 'auth:users', 'uses' => 'ProductController@postProduct']);
    $router->post('product/update', ['middleware' => 'auth:users', 'uses' => 'ProductController@updatePost']);
    $router->post('product/changeStatus', ['uses' => 'ProductController@changeStatus']);

    // load detailed hook product info
    $router->get('product/load', ['uses' => 'ProductController@loadProductforUpdate']);
});

/**
 * news
 */
$router->group(['prefix' => 'api'], function($router)
{
    $router->get('news', ['middleware' => 'cors', 'uses' => 'NewsController@feedNews']);
    $router->get('newscategory', ['middleware' => 'cors', 'uses' => 'NewsController@feedNewsByCategory']);
    $router->get('viewnews', ['middleware' => 'cors', 'uses' => 'NewsController@viewNewsArticle']);
});

/**
 * search
 */
$router->group(['prefix' => 'api'], function($router)
{
    $router->get('searchGoogle', ['middleware' => 'cors', 'uses' => 'SearchEngineController@doGoogleSearch']);
    $router->get('searchProduct', ['middleware' => 'cors', 'uses' => 'BuyAndSellController@buyAndSellSearch']);
});

/**
 * contact us
 */
$router->group(['prefix' => 'api'], function($router)
{
    $router->post('contactus', ['middleware' => 'cors', 'uses' => 'ContactUsController@createNewContact']);
});

/**
 * ads
 */
$router->group(['prefix' => 'api'], function($router)
{
    $router->post('ads/add', ['middleware' => 'cors', 'uses' => 'AdsController@createNewAds']);

    $router->get('adsload', ['middleware' => 'cors', 'uses' => 'AdsController@hookAds']);
});

/**
* admin
*/
$router->group(['prefix' => 'api', 'middleware' => 'cors'], function($router)
{
    $router->get('admin/dashboard', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getDashboard']);
    $router->get('admin/products', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getProducts']);
    $router->get('admin/products/search', ['middleware' => 'auth:admins', 'uses' => 'AdminController@searchProducts']);
    $router->get('admin/product', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getProduct']);
    $router->get('admin/users', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getUsers']);
    $router->get('admin/users/search', ['middleware' => 'auth:admins', 'uses' => 'AdminController@searchUsers']);
    $router->get('admin/user', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getUser']);
    $router->get('admin/ads', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getAds']);
    $router->get('admin/ads/search', ['middleware' => 'auth:admins', 'uses' => 'AdminController@searchAds']);
    $router->get('admin/ad', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getAd']);
    $router->get('admin/news', ['middleware' => 'auth:admins', 'uses' => 'NewsController@adminFeedNews']);
    $router->post('admin/ad/create', ['middleware' => 'auth:admins', 'uses' => 'AdminController@createAd']);
    $router->post('admin/ad/update', ['middleware' => 'auth:admins', 'uses' => 'AdminController@updateAd']);
    $router->post('admin/ad/delete', ['middleware' => 'auth:admins', 'uses' => 'AdminController@deleteAd']);
});

/**
 * comments and reply
 */
$router->group(['prefix' => 'api', 'middleware' => 'cors'], function($router)
{
    $router->post('comment/add', ['middleware' => 'auth:users', 'uses' => 'CommentController@postComment']);
    $router->post('comment/update', ['middleware' => 'auth:users', 'uses' => 'CommentController@updateComment']);
    // load comment and reply by idproduct
    $router->get('comment/load', ['uses' => 'CommentController@loadCommentAndReplybyIdproduct']);
    $router->post('comment/markread', ['middleware' => 'auth:users', 'uses' => 'CommentController@readComment']);

    // replies
    $router->post('reply/add', ['middleware' => 'auth:users', 'uses' => 'ReplyController@postReply']);
    $router->post('reply/update', ['middleware' => 'auth:users', 'uses' => 'ReplyController@updateReply']);
    $router->post('reply/markread', ['middleware' => 'auth:users', 'uses' => 'ReplyController@readReply']);
});

/**
 * admin user
 */
$router->group(['prefix' => 'api/admin', 'middleware' => 'cors'], function($router)
{
    // register admin user
    $router->post('register', ['uses' => 'AdminController@registerAdmin']);

    // update admin email
    $router->post('update/photo', ['middleware' => 'auth:admins', 'uses' => 'AdminController@updateAdminPhoto']);

    // update admin password
    $router->post('update/password', ['middleware' => 'auth:admins', 'uses' => 'AdminController@updatePassword']);

    // get admin data
    $router->get('data/load', ['middleware' => 'auth:admins', 'uses' => 'AdminController@getAdminData']);


    // login admin user
    $router->post('login', ['uses' => 'AdminController@loginAdmin']);

    // refresh admin token
    $router->get('refresh', ['uses' => 'AdminController@refresh']);

    // logout admin user
    $router->get('logout', ['uses' => 'AdminController@logoutAdmin']);
});

/**
 * to test something
 */
$router->group(['prefix' => 'api'], function($router)
{
    $router->get('test', ['middleware' => 'cors', 'uses' => 'AzureController@azureTranslate']);
});

/**
* EXCHANGE
*/
$router->group(['prefix' => 'api/exchange', 'middleware' => 'cors'], function($router)
{
    $router->get('settings', ['uses' => 'ExchangeController@getSettings']);
    $router->post('settings/save', ['uses' => 'ExchangeController@saveSettings']);
    $router->get('order-history', ['uses' => 'ExchangeController@getOrderHistory']);
    $router->get('open-orders', ['uses' => 'ExchangeController@getOpenOrders']);
    $router->get('balance', ['uses' => 'ExchangeController@getBalance']);
    $router->get('binance/listen-key', ['uses' => 'ExchangeController@getBinanceListenKey']);
});
