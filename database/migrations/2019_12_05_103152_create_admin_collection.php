<?php

use Illuminate\Support\Facades\Schema;

use Illuminate\Database\Migrations\Migration;

class CreateAdminCollection extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
