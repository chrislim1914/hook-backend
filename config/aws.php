<?php

return [
    'credentials' => [
        'key'    => env('SAW_ACCESS_KEY_ID', ''),
        'secret' => env('SAW_SECRET_ACCESS_KEY', ''),
    ],
    'region' => env('AWS_REGION', 'us-east-2'),
    'version' => 'latest',
    
    // You can override settings for specific services
    'Ses' => [
        'region' => 'us-east-2',
    ],
];
