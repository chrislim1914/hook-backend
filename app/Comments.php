<?php

namespace App;

use Validator;
use Jenssegers\Mongodb\Eloquent\Model;

class Comments extends Model
{
    protected $table = 'comment';

    /**
     * method to check if idcomments exist
     */
    static function isIdCommentExist($idcomments) {
        $isIDexist = Comments::find($idcomments)->first();
        if($isIDexist != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to validate input param when creating new comments
     */
    static function validateCommentCreate($request) {
        // lets validate
        if(array_key_exists('idcomment', $request)) {
            $validator = Validator::make($request, [
                'iduser'        => 'required',
                'idproduct'     => 'required',
                'comment'       => 'required',
                'idcomment'     => 'required',
            ]);
        }else{
            $validator = Validator::make($request, [
                'iduser'        => 'required',
                'idproduct'     => 'required',
                'comment'       => 'required',
            ]);
        }

        /**
         * me: if something went wrong on our validation then say something.
         * you: something.
         */
        if ($validator->fails()) {
            return false;
        }else{
            return true;
        }
    }

    /**
     * method to count unread comment by iduser
     *
     * @param $iduser
     * @return $countcomment
     */
    static function countUnreadComment($iduser) {
        $countcomment = Comments::where('iduser', $iduser)->where('read', 0)->count();
        return $countcomment;
    }
}
