<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = [
      'title',
      'description',
      'categoryid',
      'iduser',
      'condition',
      'meetup',
      'delivery',
      'price',
      'primary_image'
  ];

    /**
     * method to get product data
     *
     * @param $idproduct
     * @return $product
     */
    public function getProductData($idproduct) {
        $product = Product::find($idproduct);

        return $product;
    }

    /**
     * method to check if product ID exist
     *
     * @param $idproduct
     * @return Bool
     */
    public function isProductIDExist($idproduct) {
        $id_exist = Product::find($idproduct);
        if($id_exist != null){
            return true;
        }else{
            return false;
        }
    }

    public function getTotalProductCount($iduser, $viewtype) {
        if($viewtype === 'public') {
            $product = Product::where('iduser', $iduser)->where('status', 'available')->orderBy('idproduct', 'desc')->get();
        } elseif($viewtype === 'private') {
            $product = Product::where('iduser', $iduser)->orderBy('idproduct', 'desc')->get();
        }

        if($product == null) {
            return false;
        }

        return  count($product);
    }

}
