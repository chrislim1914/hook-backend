<?php

namespace App;

use Validator;
use Jenssegers\Mongodb\Eloquent\Model;

class Replies extends Model
{
    protected $table = 'reply';

    /**
     * method to check if idreply exist
     */
    static function isIdreplyExist($idreply) {
        $isIDexist = Replies::find($idreply);
        if($isIDexist != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to validate input param when creating new comments
     */
    static function validateReplyCreate($request) {
        // lets validate
        $validator = Validator::make($request, [
            'idcomment'         => 'required',
            'iduser'            => 'required',
            'reply_context'     => 'required',
        ]);

        /**
         * me: if something went wrong on our validation then say something.
         * you: something.
         */
        if ($validator->fails()) {
            return false;
        }else{
            return true;
        }
    }


    static function countUnreadReplies($iduser) {
        $countreplies = Replies::where('read', 0)->where('iduser', $iduser)->count();
        return $countreplies;
    }
}
