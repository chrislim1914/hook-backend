<?php

namespace App;

use Validator;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Jenssegers\Mongodb\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\File;


class Admin extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{

    use Authenticatable, Authorizable;
    use Notifiable;

    protected $table = 'admin';


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    /**
     * method to get admin user data
     *
     * @param $email
     * @return $admin
     */
    public function getAdminData($email) {
        $admin = Admin::where('email', $email)->first();

        return $admin;
    }

    /**
     * method to get admin folder
     *
     * @param $email
     *
     * @return $old_path
     * @return false
     */
    public function getAdminFolder($email) {
        // first we get the user info
        $gotuser = $this->getAdminData($email);

        // lets check if the profile_photo is path and not url
        $path = explode("/", $gotuser->profile_photo);
        if($path[0] === 'img') {
            return $old_path = $path[0].'/'.$path[1].'/'.$path[2].'/';
        } else {
           return false;
        }
    }

    /**
     * method to check if user email exist
     *
     * @param $email
     * @return Bool
     */
    public function isemailExist($email) {
        $email_exist = Admin::where('email', $email)->first();
        if($email_exist != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to create user folder
     *
     * @param $username
     * @return $folderdir
     * @return false
     */
    public function createAdminFolder($email) {
        // lets create time for name purpose
        $name = time();

        // lets create username by imploding email address
        $username = explode("@", $email);
        $username = preg_replace("/[^a-zA-Z0-9]/", "", $username[0]);

        $folderdir = 'img/admin/'.$username.'_'.$name.'/';

        if (file_exists($folderdir)) {
            return false;
        } else {
            File::makeDirectory($folderdir, 0777, true);
            return array(
                $folderdir,
                $username
            );
        }
    }

    /**
     * method to validate param in user registration
     *
     * @param $request
     * @return Boolean
     */
    public function validateregisterUser($request) {

        // lets validate
        $validator = Validator::make($request, [
            'email'         => 'required',
            'password'      => 'required',
        ]);

        /**
         * me: if something went wrong on our validation then say something.
         * you: something.
         */
        if ($validator->fails()) {
            return false;
        }else{
            return true;
        }

    }
}
