<?php

namespace App;

use Validator;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Jenssegers\Mongodb\Eloquent\Model;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Functions;
use PhpParser\Node\Stmt\Function_;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject
{
    use Authenticatable, Authorizable;
    use Notifiable;

    protected $table = 'user';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [];
    }

    /**
     * method to get user ID
     *
     * @param $iduser
     * @return $user
     */
    public function isIDExist($iduser) {
        $iduser = User::find($iduser);
        if($iduser != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to check if user email exist
     *
     * @param $email
     * @return Bool
     */
    public function isemailExist($email) {
        $email_exist = User::where('email', $email)->first();
        if($email_exist != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to check if user snsprodiverid exist
     *
     * @param $snsproviderid
     * @return Bool
     */
    public function issnsprovideridExist($snsproviderid) {
        $snsproviderid_exist = User::where('snsproviderid', $snsproviderid)->first();
        if($snsproviderid_exist != null){
            return true;
        }else{
            return false;
        }
    }

    /**
     * method to check if username exist
     *
     * @param $username
     * @return Bool
     */
    public function isUsernameExist($username, $iduser = null) {
        if($iduser == null) {
            $username_exist = User::where('username', $username)->first();
            if($username_exist != null){
                return true;
            }else{
                return false;
            }
        }else{
            $username_exist = User::where('username', $username)->where('_id', '<>', $iduser)->get();
            if($username_exist->count() !== 0){
                return false;
            }else{
                return true;
            }
        }
    }

    /**
     * method to create user folder
     *
     * @param $username
     * @return $folderdir
     * @return false
     */
    public function createUserFolder($username) {
        // lets create time for name purpose
        $name = time();

        $folderdir = 'img/user/'.$username.'_'.$name.'/';

        if (file_exists($folderdir)) {
            return false;
        } else {
            File::makeDirectory($folderdir, 0777, true);
            return $folderdir;
        }
    }

    /**
     * method to create user folder for product
     *
     * @param $username
     * @return $folderdir
     * @return false
     */
    public function createUserFolderProduct($path, $id) {
        // lets create time for name purpose
        $name = time();

        $folderdir = $path.'product_'.$id.'/';

        if (file_exists($folderdir)) {
            return false;
        } else {
            File::makeDirectory($folderdir, 0777, true);
            return $folderdir;
        }
    }

    /**
     * method to get user image folder
     *
     * @param $iduser
     *
     * @return $old_path
     * @return false
     */
    public function getUserFolder($iduser) {
        // first we get the user info
        $gotuser = User::find($iduser);

        // lets check if the profile_photo is path and not url
        $path = explode("/", $gotuser->profile_photo);
        if($path[0] === 'img') {
            return $old_path = $path[0].'/'.$path[1].'/'.$path[2].'/';
        } else {
           return false;
        }
    }

    public function profilePath($pathphoto) {
        $function = new Functions();
        $baseURL = $function->getAppURL();
        $path = explode("/", $pathphoto);
        if($path[0] === 'img') {
            return $baseURL.$pathphoto;
        } else {
           return $pathphoto;
        }
    }

    /**
     * method to validate param in SNS signin and signup
     *
     * @param $request
     * @return Boolean
     */
    public function validateSignupSignin($request) {
        $user = new User();

        // lets validate
        $validator = Validator::make($request, [
            'email'         => 'required',
            'firstname'     => 'required',
            'lastname'      => 'required',
            'profile_photo' => 'required',
            'snsproviderid' => 'required',
        ]);
        /**
         * me: if something went wrong on our validation then say something.
         * you: something.
         */
        if ($validator->fails()) {
            return false;
        }else{
            return true;
        }

    }

    /**
     * method to validate param in user registration
     *
     * @param $request
     * @return Boolean
     */
    public function validateregisterUser($request) {
        $user = new User();

        // lets validate
        $validator = Validator::make($request, [
            'email'         => 'required',
            'firstname'     => 'required',
            'lastname'      => 'required',
            'username'      => 'required',
            'password'      => 'required',
            'contactno'      => 'required',
        ]);
        /**
         * me: if something went wrong on our validation then say something.
         * you: something.
         */
        if ($validator->fails()) {
            return false;
        }else{
            return true;
        }

    }
}
