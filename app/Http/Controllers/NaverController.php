<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class NaverController extends Controller
{
    private $n2mt_url = 'https://naveropenapi.apigw.ntruss.com/nmt/v1/translation';
    private $sourceLang = 'en';

    public function naverTranslate(Request $request) {
        // $s = $this->curlCall($request->text, $request->countrycode);
    }

    protected function curlCall($text, $countrycode) {

        $key = $this->getCredential();

        $fielddata = 'source='. $this->sourceLang . '&target=' . $countrycode . '&text=' . $text;
        $header = array(
            "X-NCP-APIGW-API-KEY-ID: $key[0]",
            "X-NCP-APIGW-API-KEY-ID: $key[1]"
        );
        var_dump($header);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$this->n2mt_url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fielddata);        
        $result=curl_exec ($ch);
        curl_close ($ch);

        $jsonlist = json_decode($result, true);
        var_dump($jsonlist);
    }

    /**
     * method to get openweather app key
     * 
     * @return $openweatherapikey
     */
    protected function getCredential() {        
        $idkey  = env('PN_ID');
        $skey  = env('PN_SID');
        return array (
            $idkey,
            $skey
        );
    }
}
