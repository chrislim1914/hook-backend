<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\CarousellController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ShopeeController;
use App\Http\Controllers\ScrapController;
use App\Http\Controllers\Functions;
use App\Product;

class BuyAndSellController extends Controller
{

    private $carousell;
    private $hook;
    private $shopee;
    private $function;

    /**
     * instantiate CarousellController, ProductController, Functions
     */
    public function __construct(CarousellController $carousell, ProductController $hook, ShopeeController $shopee, Functions $function) {
        $this->carousell    = $carousell;
        $this->hook         = $hook;
        $this->shopee       = $shopee;
        $this->function     = $function;
    }

    protected function translateBuyAndSellSearch($content, $countrycode, $source) {
        if($countrycode === 'en') {
            return $content;
        }
        $translatedData = [];
        foreach($content['data'] as $new) {
            $snippet = [];
            for($i=0;$i<4;$i++) {
                $snippet[] = $new['snippet'][$i] == "" ? '' : $this->function->translator($new['snippet'][$i], $countrycode);
            }
            array_push($translatedData, [
                'id'                =>  $new['id'],
                'title'             =>  $new['title'] == '' ? '' : $this->function->translator($new['title'], $countrycode),
                'snippet'           =>  $snippet,
                'link'              =>  $new['link'],
                'image'             =>  $new['image'],
                'thumbnailimage'    =>  $new['thumbnailimage'],
                'source'            =>  $new['source'],
            ]);
        }
        // array_push($translatedData['total'], $content['total']);
        // array_push($translatedData['result'], $content['result']);
        return array(
            'data'      => $translatedData,
            'total'     => $content['total'],
            'result'    => $content['result']
        );
    }

    /**
     * method to get $request->countrycode
     *
     * @param $request
     * @return $viewitem
     * @return Boolean
     */
    protected function translateViewContent($content, $countrycode, $source) {
        if($countrycode === 'en') {
            return $content;
        }

        $viewitem = [];

        switch ($source) {
            case 'carousell':
                $viewitem = [
                    'url'               => $content['url'],
                    'seller'            => $content['seller'],
                    'media'             => $content['media'],
                    'itemname'          => $this->function->translator($content['itemname'], $countrycode),
                    'price'             => $content['price'],
                    'description'       => $this->function->translator($content['description'], $countrycode),
                    'source'            => $content['source'],
                    'similar_item'      => $content['similar_item'],
                ];

                return $viewitem;

            case 'hook':
                $viewitem = [
                    'url'               => $content['url'],
                    'seller'            => $content['seller'],
                    'media'             => $content['media'],
                    'itemname'          => $this->function->translator($content['itemname'], $countrycode),
                    'price'             => $content['price'],
                    'description'       => $this->function->translator($content['description'], $countrycode),
                    'condition'         => $this->function->translator($content['condition'], $countrycode),
                    'meetup'            => $content['meetup'] == '' ? '' : $this->function->translator($content['meetup'], $countrycode),
                    'delivery'          => $content['delivery'] == '' ? '' : $this->function->translator($content['delivery'], $countrycode),
                    'status'            => $content['status'],
                    'source'            => $content['source'],
                    'similar_item'      => $content['similar_item'],
                ];
                return $viewitem;
        }
    }

    /**
     * method to translate mergeFrontDisplay() method
     *
     * @param $buyandselldata, $countrycode
     * @return $translatedData
     */
    protected function translateFrontDIsplay($buyandselldata, $countrycode) {
        if($countrycode === 'en') {
            return $buyandselldata;
        }

        $translatedData = [];
        foreach($buyandselldata as $new) {
            $info = [];
            for($i=0;$i<4;$i++) {
                $info[] = [
                    'stringContent' => $new['info'][$i]['stringContent'] == "" ? '' : $this->function->translator($new['info'][$i]['stringContent'], $countrycode),
                ];
            }

            array_push($translatedData, [
                'id'            =>  $new['id'],
                'seller'        =>  $new['seller'],
                'photoUrls'     =>  $new['photoUrls'],
                'info'          =>  $info,
                'source'        =>  $new['source'],
            ]);
        }

        return $translatedData;
    }

    /**
     * method to translate translateFeedBuyandSell() method
     *
     * @param $buyandselldata, $countrycode
     * @return $translatedData
     */
    protected function translateFeedBuyandSell($buyandselldata, $countrycode) {
        if($countrycode === 'en') {
            return $buyandselldata;
        }

        $translatedData = [];
        foreach($buyandselldata as $new) {
            $snippet = [];
            for($i=0;$i<4;$i++) {
                $snippet[] = $new['snippet'][$i] == "" ? '' : $this->function->translator($new['snippet'][$i], $countrycode);
            }
            array_push($translatedData, [
                'id'                =>  $new['id'],
                'title'             =>  $new['title'] == '' ? '' : $this->function->translator($new['title'], $countrycode),
                'snippet'           =>  $snippet,
                'link'              =>  $new['link'],
                'image'             =>  $new['image'],
                'thumbnailimage'    =>  $new['thumbnailimage'],
                'source'            =>  $new['source'],
            ]);
        }

        return $translatedData;
    }

    /**
     * method to display cvarousell and our product in the front page
     *
     * @return $buyandsell
     */
    public function mergeFrontDisplay(Request $request) {
        $countrycode = $this->function->isThereCountryCode($request);

        $front_carousell    = $this->carousell->getCarousell();
        $front_hook         = $this->hook->loadOurProduct();
        $front_shopee       = $this->shopee->getShopee();
        $buyandsell = [];

        for($i=0;$i<5;$i++) {

            if($front_hook) {
                foreach($front_hook as $hook) {
                    if($i == $hook['no']) {
                        array_push($buyandsell, [
                            'id'            =>  $hook['id'],
                            'seller'        =>  $hook['seller'],
                            'photoUrls'     =>  $hook['photoUrls'],
                            'info'          =>  $hook['info'],
                            'source'        =>  'hook'
                        ]);
                    }
                }
            }

            // if($front_carousell) {
            //     foreach($front_carousell as $carousell) {
            //         if($i == $carousell['no']) {
            //             array_push($buyandsell, [
            //                 'id'            =>  $carousell['id'],
            //                 'seller'        =>  $carousell['seller'],
            //                 'photoUrls'     =>  $carousell['photoUrls'],
            //                 'info'          =>  $carousell['info'],
            //                 'source'        =>  'carousell'
            //             ]);
            //         }
            //     }
            // }

            // if($front_shopee) {
            //     foreach($front_shopee as $shopee) {
            //         if($i == $shopee['no']) {
            //             array_push($buyandsell, [
            //                 'id'            =>  $shopee['id'],
            //                 'seller'        =>  $shopee['seller'],
            //                 'photoUrls'     =>  $shopee['photoUrls'],
            //                 'info'          =>  $shopee['info'],
            //                 'shopid'        =>  $shopee['shopid'],
            //                 'source'        =>  'shopee'
            //             ]);
            //         }
            //     }
            // }
        }

        if(!$countrycode){
            return response()->json([
                'data'      => $buyandsell,
                'result'    => true
            ]);
        }

        $trans = $this->translateFrontDIsplay($buyandsell, $countrycode);

        return response()->json([
            'data'      => $trans,
            'result'    => true
        ]);
    }

    /**
     * method to display cvarousell and our product in the buy and sell page
     *
     * @param Request $request
     * @return $buyandsell
     */
    public function feedBuyandSell(Request $request) {

        $countrycode = $this->function->isThereCountryCode($request);

        $feedcarousell  = $this->carousell->feedCarousell($request->page);
        $feedhook       = $this->hook->feedHook($request->page);
        $feedshopee     = $this->shopee->feedShopee($request->page);

        $buyandsell = [];
        for($i=0;$i<10;$i++) {

            if($feedhook) {
                foreach($feedhook as $hook) {
                    if($i == $hook['no']) {
                        array_push($buyandsell, [
                            'id'                =>  $hook['id'],
                            'title'             =>  $hook['title'],
                            'snippet'           =>  $hook['snippet'],
                            'link'              =>  $hook['link'],
                            'image'             =>  $hook['image'],
                            'thumbnailimage'    =>  $hook['thumbnailimage'],
                            'source'            =>  $hook['source'],
                        ]);
                    }
                }
            }

            // if($feedcarousell) {
            //     foreach($feedcarousell as $carousell) {
            //         if($i == $carousell['no']) {
            //             array_push($buyandsell, [
            //                 'id'                =>  $carousell['id'],
            //                 'title'             =>  $carousell['title'],
            //                 'snippet'           =>  $carousell['snippet'],
            //                 'link'              =>  $carousell['link'],
            //                 'image'             =>  $carousell['image'],
            //                 'thumbnailimage'    =>  $carousell['thumbnailimage'],
            //                 'source'            =>  $carousell['source'],
            //             ]);
            //         }
            //     }
            // }

            // if($feedshopee) {
            //     foreach($feedshopee as $shopee) {
            //         if($i == $shopee['no']) {
            //             array_push($buyandsell, [
            //                 'id'                =>  $shopee['id'],
            //                 'title'             =>  $shopee['title'],
            //                 'snippet'           =>  $shopee['snippet'],
            //                 'link'              =>  $shopee['link'],
            //                 'image'             =>  $shopee['image'],
            //                 'thumbnailimage'    =>  $shopee['thumbnailimage'],
            //                 'shopid'            =>  $shopee['shopid'],
            //                 'source'            =>  $shopee['source'],
            //             ]);
            //         }
            //     }
            // }
        }

        if(!$countrycode){
            return response()->json([
                'data'      => $buyandsell,
                'result'    => true
            ]);
        }

        $trans = $this->translateFeedBuyandSell($buyandsell, $countrycode);

        return response()->json([
            'data'      => $trans,
            'result'    => true
        ]);
    }

    /**
     * method to view single product from hook, and carousell
     *
     * @param Request $request
     * @return JSON
     */
    public function viewSingleContent(Request $request) {
        $countrycode = $this->function->isThereCountryCode($request);
        $source = $request->source;

        // $scrap = new ScrapController();
        $product = new ProductController();

        switch ($source) {
            case 'carousell':
                $view_carousell = $this->carousell->viewCarousell($request->id);
                $categoryid = $this->carousell->carousellKeywords($view_carousell['keyword']);
                if($categoryid == null) {
                    // lets use the meta_keywords key
                    $categoryid = $this->carousell->carousellKeywords($view_carousell['meta_keywords']);
                }
                $similaritem = $this->buyAndSellFilter($countrycode, 1, $view_carousell['keyword'], array(strval($categoryid)));
                $view_carousell['similar_item'] = $similaritem['data'];

                if(!$countrycode){
                    return response()->json([
                        'data'          => $view_carousell,
                        'result'        => true
                    ]);
                }

                $trans = $this->translateViewContent($view_carousell, $countrycode, $source);

                return response()->json([
                    'data'          => $trans,
                    'result'        => true
                ]);

            case 'hook':
                $productmpdel = new Product();
                // lets check if hook idproduct exist
                if(!$productmpdel->isProductIDExist($request->id)){
                    return response()->json([
                        'message'       => 'Product not Found!',
                        'result'        => false
                    ]);
                }
                $view_product = $product->viewProduct($request->id);
                $similaritem = $this->buyAndSellFilter($countrycode, 1, $view_product['itemname'], array(strval($view_product['category'])), $request->id);
                $view_product['similar_item'] = $similaritem['data'];

                if(!$countrycode){
                    return response()->json([
                        'data'          => $view_product,
                        'result'        => true
                    ]);
                }

                $trans = $this->translateViewContent($view_product, $countrycode, $source);

                return response()->json([
                    'data'          => $trans,
                    'result'        => true
                ]);
            case 'shopee':
                $view_shopee = $this->shopee->viewShopee($request->id, $request->shopid);
                // on similar items we wont use the method buyAndSellFilter()
                // instead we're going to use shopeeOnlyFilter() but actually it a search
                // using the product name
                $similaritem = $this->shopeeOnlyFilter(1, $view_shopee['keyword']);
                $view_shopee['similar_item'] = $similaritem['data'];
                if(!$countrycode){
                    return response()->json([
                        'data'          => $view_shopee,
                        'result'        => true
                    ]);
                }

                $trans = $this->translateViewContent($view_shopee, $countrycode, $source);

                return response()->json([
                    'data'          => $trans,
                    'result'        => true
                ]);
        }
    }

    /**
     * merge search method for carousell and hook
     *
     * @param $request
     * @return JSON
     */
    public function buyAndSellSearch(Request $request) {
        $countrycode = $this->function->isThereCountryCode($request);
        $source = $request->source;
        $page = $request->page;
        $request->has('search') == true ? $search = $request->search : $search = '';
        $request->has('filter') == true ? $filter = $request->filter : $filter = '';

        switch ($source) {
            case 'carousell';
                $searchcar = $this->carousell->doCarousellSearch($page, $search, $filter);
                if($searchcar == false) {
                    return response()->json([
                        'message'   => [],
                        'result'    => false
                    ]);
                }
                if(array_key_exists('total', $searchcar )) {
                    if(!$countrycode){
                        return response()->json([
                            'data'      => $searchcar['data'],
                            'total'     => $searchcar['total'],
                            'result'    => $searchcar['result']
                        ]);
                    }
                    $searchdata = $this->translateBuyAndSellSearch($searchcar, $countrycode, $source);
                    return response()->json([
                        'data'      => $searchdata['data'],
                        'total'     => $searchdata['total'],
                        'result'    => $searchdata['result']
                    ]);
                } else {
                    return response()->json([
                        'message'   => $searchcar['data'],
                        'result'    => $searchcar['result']
                    ]);
                }
                break;
            case 'hook':
                $searchhook = $this->hook->searchProduct($page, $search);
                if($searchhook == false) {
                    return response()->json([
                        'message'   => [],
                        'result'    => false
                    ]);
                }
                if(array_key_exists('total', $searchhook )) {
                    if(!$countrycode){
                        return response()->json([
                            'data'      => $searchhook['data'],
                            'total'     => $searchhook['total'],
                            'result'    => $searchhook['result']
                        ]);
                    }
                    $searchdata = $this->translateBuyAndSellSearch($searchhook, $countrycode, $source);
                    return response()->json([
                        'data'      => $searchdata['data'],
                        'total'     => $searchdata['total'],
                        'result'    => $searchdata['result']
                    ]);
                } else {
                    return response()->json([
                        'message'   => $searchhook['data'],
                        'result'    => $searchhook['result']
                    ]);
                }
                break;
            case 'shopee':
                $searchshopee = $this->shopee->doShopeeSearch($page, $search);
                if($searchshopee == false) {
                    return response()->json([
                        'message'   => [],
                        'result'    => false
                    ]);
                }
                if(array_key_exists('total', $searchshopee )) {
                    if(!$countrycode){
                        return response()->json([
                            'data'      => $searchshopee['data'],
                            'total'     => $searchshopee['total'],
                            'result'    => $searchshopee['result']
                        ]);
                    }
                    $searchdata = $this->translateBuyAndSellSearch($searchshopee, $countrycode, $source);
                    return response()->json([
                        'data'      => $searchdata['data'],
                        'total'     => $searchdata['total'],
                        'result'    => $searchdata['result']
                    ]);
                } else {
                    return response()->json([
                        'message'   => $searchshopee['data'],
                        'result'    => $searchshopee ['result']
                    ]);
                }
                break;
            default:
                return response()->json([
                    'message'   => 'Source is empty!',
                    'result'    => false
                ]);
        }

    }

    public function buyAndSellFilterForPage(Request $request) {
        $page = $request->page;
        $request->has('countrycode') == true ? $countrycode = $request->countrycode : $countrycode = '';
        $request->has('search') == true ? $search = $request->search : $search = '';
        $request->has('filter') == true ? $filter = $request->filter : $filter = '';

        $got_data = $this->buyAndSellFilter($countrycode, $page, $search, $filter);

        return response()->json([
            'data'      => $got_data['data'],
            'result'    => $got_data['result']
        ]);
    }

    public function shopeeOnlyFilter($page, $search) {
        $filter = '';
        $searchForHook = $this->hook->searchProduct($page, $search);
        $searchForCarousell = $this->carousell->doCarousellSearch($page, $search,  $filter);
        $searchForShopee = $this->shopee->doShopeeSearch($page, $search);
        $similarItems = [];

        for($i=0;$i<10;$i++) {

            if($searchForHook) {
                foreach($searchForHook['data'] as $hook) {
                    if($i == $hook['no']) {
                        array_push($similarItems, [
                            'id'                =>  $hook['id'],
                            'shopid'            =>  0,
                            'title'             =>  $hook['title'],
                            'snippet'           =>  $hook['snippet'],
                            'link'              =>  $hook['link'],
                            'image'             =>  $hook['image'],
                            'thumbnailimage'    =>  $hook['thumbnailimage'],
                            'source'            =>  $hook['source'],
                        ]);
                    }
                }
            }

            if($searchForCarousell) {
                foreach($searchForCarousell['data'] as $carousell) {
                    if($i == $carousell['no']) {
                        array_push($similarItems, [
                            'id'                =>  $carousell['id'],
                            'shopid'            =>  0,
                            'title'             =>  $carousell['title'],
                            'snippet'           =>  $carousell['snippet'],
                            'link'              =>  $carousell['link'],
                            'image'             =>  $carousell['image'],
                            'thumbnailimage'    =>  $carousell['thumbnailimage'],
                            'source'            =>  $carousell['source'],
                        ]);
                    }
                }
            }

            if($searchForShopee) {
                foreach($searchForShopee['data'] as $shopeeitem) {
                    if($i == $shopeeitem['no']) {
                        array_push($similarItems, [
                            'id'                =>  $shopeeitem['id'],
                            'shopid'            =>  $shopeeitem['shopid'],
                            'title'             =>  $shopeeitem['title'],
                            'snippet'           =>  $shopeeitem['snippet'],
                            'link'              =>  $shopeeitem['link'],
                            'image'             =>  $shopeeitem['image'],
                            'thumbnailimage'    =>  $shopeeitem['thumbnailimage'],
                            'source'            =>  $shopeeitem['source'],
                        ]);
                    }
                }
            }
        }

        return array(
            'data'      => $similarItems,
            'result'    => true
        );
    }

    /**
     * filter method for carousell and hook
     *
     * @param $request
     * @return JSON
     */
    public function buyAndSellFilter($countrycode, $page, $search, $filter, $idproduct = '') {
        $filter_carousell    = $this->carousell->filterCarousell($page, '', $filter);
        $filterhook          = $this->hook->filterProduct($filter, $page, $idproduct);
        // shopee we will use doShopeeSearch()
        $shopeesearch        = $this->shopee->doShopeeSearch($page, $search);
        
        $buyandsellfilter = [];
        for($i=0;$i<10;$i++) {

            if($filterhook) {
                foreach($filterhook as $hook) {
                    if($i == $hook['no']) {
                        array_push($buyandsellfilter, [
                            'id'                =>  $hook['id'],
                            'shopid'            =>  0,
                            'title'             =>  $hook['title'],
                            'snippet'           =>  $hook['snippet'],
                            'link'              =>  $hook['link'],
                            'image'             =>  $hook['image'],
                            'thumbnailimage'    =>  $hook['thumbnailimage'],
                            'source'            =>  $hook['source'],
                        ]);
                    }
                }
            }

            // if($filter_carousell) {
            //     foreach($filter_carousell as $carousell) {
            //         if($i == $carousell['no']) {
            //             array_push($buyandsellfilter, [
            //                 'id'                =>  $carousell['id'],
            //                 'shopid'            =>  0,
            //                 'title'             =>  $carousell['title'],
            //                 'snippet'           =>  $carousell['snippet'],
            //                 'link'              =>  $carousell['link'],
            //                 'image'             =>  $carousell['image'],
            //                 'thumbnailimage'    =>  $carousell['thumbnailimage'],
            //                 'source'            =>  $carousell['source'],
            //             ]);
            //         }
            //     }
            // }

            // if($shopeesearch) {
            //     foreach($shopeesearch['data'] as $shopeeitem) {
            //         if($i == $shopeeitem['no']) {
            //             array_push($buyandsellfilter, [
            //                 'id'                =>  $shopeeitem['id'],
            //                 'shopid'            =>  $shopeeitem['shopid'],
            //                 'title'             =>  $shopeeitem['title'],
            //                 'snippet'           =>  $shopeeitem['snippet'],
            //                 'link'              =>  $shopeeitem['link'],
            //                 'image'             =>  $shopeeitem['image'],
            //                 'thumbnailimage'    =>  $shopeeitem['thumbnailimage'],
            //                 'source'            =>  $shopeeitem['source'],
            //             ]);
            //         }
            //     }
            // }
        }

        if($countrycode == ''){
            return array(
                'data'      => $buyandsellfilter,
                'result'    => true
            );
        }

        $trans = $this->translateFeedBuyandSell($buyandsellfilter, $countrycode);

        return array(
            'data'      => $trans,
            'result'    => true
        );
    }
}
