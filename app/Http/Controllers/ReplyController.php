<?php

namespace App\Http\Controllers;

use App\Replies;
use App\Comments;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    private $reply;
    private $comment;

    public function __construct(Replies $reply, Comments $comment) {
        $this->reply = $reply;
        $this->comment = $comment;
    }

    public function postReply(Request $request) {
        // check input param
        $checkparam = $this->reply::validateReplyCreate($request->all());
        if(!$checkparam) {
            return response()->json([
                'message'   => 'Input parameter is incorrect!',
                'result'    => false
            ]);
        }

        // prepare the data to insert
        $this->reply->idcomment     = $request->idcomment;
        $this->reply->iduser        = $request->iduser;
        $this->reply->reply_context = $request->reply_context;
        $this->reply->read          = 0;
        $this->reply->delete        = 0;

        if($this->reply->save()) {
            return response()->json([
                'message'   => '',
                'result'    => true
            ]);
        }else{
            return response()->json([
                'message'   => 'Failed to save reply!',
                'result'    => false
            ]);
        }
    }

    public function updateReply(Request $request) {
        // check if idcomment
        $checkid = $this->reply::isIdreplyExist($request->idreply);
        if(!$checkid) {
            return response()->json([
                'message'   => 'Reply info not found!',
                'result'    => false
            ]);
        }

        // check input param
        $checkparam = $this->reply::validateReplyCreate($request->all());
        if(!$checkparam) {
            return response()->json([
                'message'   => 'Input parameter is incorrect!',
                'result'    => false
            ]);
        }

        $updatethisreply = $this->reply::where('idreply', $request->idreply);
        $updatethisreply->update([
            'idcomment'     =>  $request->idcomment,
            'reply_context' =>  $request->reply_context
        ]);

        return response()->json([
            'message'   => '',
            'result'    => true
        ]);
    }

    public function readReply(Request $request) {
        // check if idcomment
        $checkid = $this->reply::isIdreplyExist($request->idreply);
        if(!$checkid) {
            return response()->json([
                'message'   => 'Reply info not found!',
                'result'    => false
            ]);
        }

        $markasread = $this->reply::where('idreply', $request->idreply);
        $markasread->update([
            'read'       => 1,
        ]);

        return response()->json([
            'message'   => '',
            'result'    => true
        ]);
    }
}
