<?php

namespace App\Http\Controllers;
use App\User;
use App\Product;
use App\Comments;
use App\Replies;
use Illuminate\Http\Request;
use App\Http\Controllers\Functions;

class CommentController extends Controller
{
    private $user;
    private $product;
    private $comment;

    /**
     * Instantiate User, Product, Comment
     */
    public function __construct(User $user, Product $product, Comments $comment) {
        $this->user = $user;
        $this->product = $product;
        $this->comment = $comment;
    }

    /**
     * method to post new comment
     *
     * @param Request $request
     * @return JSON
     */
    public function postComment(Request $request) {
        // check all
        $checkAll = $this->checkComment($request->all());
        if($checkAll['result'] == false) {
            return response()->json([
                'message'   => $checkAll['message'],
                'result'    => $checkAll['result']
            ]);
        }

        // prepare the data
        $this->comment->iduser      = $request->iduser;
        $this->comment->idproduct   = $request->idproduct;
        $this->comment->comment     = $request->comment;
        $this->comment->read        = 0;
        $this->comment->delete      = 0;

        if($this->comment->save()) {
            return response()->json([
                'message'   => '',
                'result'    => true
            ]);
        }else{
            return response()->json([
                'message'   => 'Failed to save comment!',
                'result'    => false
            ]);
        }
    }

    /**
     * method to update comment
     *
     * @param Request $request
     * @return JSON
     */
    public function updateComment(Request $request) {
        if(!$request->has('idcomment')) {
            return response()->json([
                'message'   => 'Input parameter is incorrect!',
                'result'    => false
            ]);
        }
        // add idcomment to $request->all()
        $request->all()['idcomment'] = $request->idcomment;
        $checkAll = $this->checkComment($request->all());
        if($checkAll['result'] == false) {
            return response()->json([
                'message'   => $checkAll['message'],
                'result'    => $checkAll['result']
            ]);
        }

        // lets update
        $updatecomment = $this->comment::where('idcomments', $request->idcomment);
        $updatecomment->update([
            'comment'       => $request->comment,
        ]);
        return response()->json([
            'message'   => '',
            'result'    => true
        ]);
    }

    /**
     * method to mark comment as read
     *
     * @param Request $request
     * @return JSON
     */
    public function readComment(Request $request) {
        if(!$request->has('idcomment')) {
            return response()->json([
                'message'   => 'Input parameter is incorrect!',
                'result'    => false
            ]);
        }
        // add idcomment to $request->all()
        $checkidcomment = $this->comment::isIdCommentExist($request->idcomment);
        if(!$checkidcomment) {
            return response()->json([
                'message'   => 'Comment info not found!',
                'result'    => false
            ]);
        }

        // lets update
        $markasread = $this->comment::where('idcomments', $request->idcomment);
        $markasread->update([
            'read'       => 1,
        ]);
        return response()->json([
            'message'   => '',
            'result'    => true
        ]);

    }

    /**
     * method to load comment and replies
     *
     * @param Request $request
     * @return JSON
     */
    public function loadCommentAndReplybyIdproduct(Request $request) {
        // check idproduct
        $function = new Functions();
        $baseURL    = $function::getAppURL();

        $checkid = $this->product->isProductIDExist($request->idproduct);
        if(!$checkid) {
            return response()->json([
                'message'   => 'product info not found!',
                'result'    => false
            ]);
        }

        // now lets get the comment and insert if there is reply
        $commentlist = $this->comment::where('idproduct', $request->idproduct)->orderby('created_at', 'desc')->get();

        $commentlistdata = [];
        foreach($commentlist as $newdata) {
            // prepare user info by id
            $userdata = User::find($newdata['iduser']);

            // insert if there is repies on comment
            $replies = [];
            $replylist = Replies::where('idcomment', $newdata['_id'])->orderby('created_at', 'desc')->get();
            foreach($replylist as $replydata) {
                $replyuser = User::find($replydata['iduser']);
                $replies[] = [
                    'idreply'       => $replydata['_id'],
                    'iduser'        => $replyuser['_id'],
                    'username'      => $replyuser['username'],
                    'profile_photo' => $baseURL.$replyuser['profile_photo'],
                    'reply'         => $replydata['reply_context'],
                    'posted_at'     => $function->timeLapse($replydata['created_at'])
                ];
            }

            $commentlistdata[] = [
                'idcomments'    => $newdata['_id'],
                'iduser'        => $newdata['iduser'],
                'username'      => $userdata['username'],
                'profile_photo' => $baseURL.$userdata['profile_photo'],
                'comment'       => $newdata['comment'],
                'posted_at'     => $function->timeLapse($newdata['created_at']),
                'replies'       => $replies,
            ];
        }

        return response()->json([
            'data'      => $commentlistdata,
            'result'    => true
        ]);
    }

    /**
     * method to check:
     *  input param
     *  iduser
     *  idproduct
     *  idcomment
     * @param $request
     * @return Array
     */
    protected function checkComment($request) {
        // check input param
        $checkInput = $this->comment::validateCommentCreate($request);
        if(!$checkInput) {
            return array(
                'message'   => 'Input parameter is incorrect!',
                'result'    => false
            );
        }

        // check iduser
        $checkIduser = $this->user->isIDExist($request['iduser']);
        if(!$checkIduser) {
            return array(
                'message'   => 'User not found!',
                'result'    => false
            );
        }

        // check idproduct
        $checkIdproduct = $this->product->isProductIDExist($request['idproduct']);
        if(!$checkIdproduct) {
            return array(
                'message'   => 'Product not found!',
                'result'    => false
            );
        }

        if(array_key_exists('idcomment', $request)) {
            $checkidcomment = $this->comment::isIdCommentExist($request['idcomment']);
            if(!$checkidcomment) {
                return array(
                    'message'   => 'ID comment not found!',
                    'result'    => false
                );
            }else{
                return array(
                    'message'   => '',
                    'result'    => true
                );
            }
        }else{
            return array(
                'message'   => '',
                'result'    => true
            );
        }
    }
}
