<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AzureController extends Controller
{
    //
    private $azureEndPoint = 'https://api.cognitive.microsofttranslator.com';
    private $path = '/translate?api-version=3.0';

    public function azureTranslate($text, $countrycode) {
        $params = '&to='.$countrycode.'&textType=html';

        $requestBody = array (
            array (
                'Text' => $text,
            ),
        );

        $content = json_encode($requestBody);

        $headers = array(
            "content-type: application/json",
            "Ocp-Apim-Subscription-Key: ".$this->getKey(),
        );
    
        $options = array (
            'http' => array (
                'header' => $headers,
                'method' => 'POST',
                'content' => $content
            )
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->azureEndPoint . $this->path . $params);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout after 30 seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);        
        $result=curl_exec ($ch);
        curl_close ($ch);

        $json = json_decode($result, true);
        
        foreach($json as $newdata) {
            if (array_key_exists('code', $newdata)) {
                return;
            }
            $newTrans = $newdata['translations'][0]['text'];
        }
        return $newTrans;
    }

    protected function getKey() {
        if (!getenv("AKEY")) {
            return false;
        } else {
            return getenv("AKEY");
        }
    }
}
