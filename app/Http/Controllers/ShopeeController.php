<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Client;
use App\Http\Controllers\Functions;
use Illuminate\Support\Facades\Config;

class ShopeeController extends Controller
{
    private $shopee_url = 'https://shopee.ph/api/v2/recommend_items/get';
    private $shopeesearch_url = 'https://shopee.ph/api/v2/search_items/';
    private $shopeeimg_url = 'https://cf.shopee.ph/file/';
    private $shopeeviewitm_url = 'https://shopee.ph/api/v2/item/get?';
    private $shopeeseller_url = 'https://shopee.ph/api/v2/shop/get?is_brief=1&shopid=';
    private $shopeesimilaritem_url = 'https://shopee.ph/api/v2/recommend_items/get?';

    public function getShopee() {
        $function = new Functions();
        $url = $this->shopee_url.'?limit=5&offset=5&recommend_type=5';
        
        $shopeeCall = $function->guzzleHttpCall($url);

        if($shopeeCall == false) {
            return false;
        }
        // var_dump(count($shopeeCall['data']['items']));
        
        if(count($shopeeCall['data']['items']) > 0) {
            $shopeefeed = [];
            $count = 0;
                foreach($shopeeCall['data']['items'] as $sfeed) {
                    $info = [];

                    $info1 = [
                        'stringContent' => $sfeed['name'],
                    ];
        
                    $info2 = [
                        'stringContent' => 'PHP ' . substr($sfeed['price'], 0, -5),
                    ];
                    $info3 = [
                        'stringContent' => $sfeed['name'],
                    ];
        
                    $info4 = [
                        'stringContent' => NULL,
                    ];
        
                    $info = [
                        $info1,
                        $info2,
                        $info3,
                        $info4,
                    ];

                    $shopeefeed[] = [
                        'no'            =>  $count,
                        'id'            =>  $sfeed['itemid'],
                        'seller'        =>  [],
                        'photoUrls'     =>  [
                                            $this->shopeeimg_url.$sfeed['image']
                                        ],
                        'info'          =>  $info,
                        'shopid'        =>  $sfeed['shopid'],
                        'source'        =>  'Shopee'
                    ];

                    $count++;
                }
                return $shopeefeed;
        } else {
            return false;
        }
    }

    public function feedShopee($page) {

        $function = new Functions();
        $url = $this->shopee_url.'?limit=10&offset='. $page * 10 .'&recommend_type=5';
        
        $shopeeCall = $function->guzzleHttpCall($url);

        if($shopeeCall == false) {
            return false;
        }
        
        if(count($shopeeCall['data']['items']) > 0) {
            $shopee = $this->createCarousellData($shopeeCall);
            return $shopee['data'];
        } else {
            return false;
        }

    }

    public function viewShopee($id, $shopid) {
        $function = new Functions();

        // item data
        $url = $this->shopeeviewitm_url.'itemid='. $id .'&shopid=' .$shopid;
        $shopeeCall = $function->guzzleHttpCall($url);
        
        // seller data
        $seller = $this->shopeeseller_url.$shopid;
        $shopeeSeller = $function->guzzleHttpCall($seller);
        if($shopeeCall == false && $shopeeSeller == false) {
            return false;
        }
        
        $newshopee_item = [];
        $seller = [
            'id'            => $shopeeSeller['data']['shopid'],
            'username'      => $shopeeSeller['data']['account']['username'],
            'profile_photo' => $this->shopeeimg_url.$shopeeSeller['data']['account']['portrait'],
        ];
        
        $media = [];
        foreach($shopeeCall['item']['images'] as $img) {
            $media[] = $this->shopeeimg_url.$img;
        }

        $newshopee_item = [
            'url'               => 'https://shopee.ph/'.$this->treatTitle($shopeeCall['item']['name']).'-i.'.$shopid.'.'.$id,
            'seller'            => $seller,
            'category'          => $shopeeCall['item']['categories'][0]['catid'], 
            'media'             => $media,           
            'itemname'          => $shopeeCall['item']['name'],
            'price'             => 'PHP ' . substr($shopeeCall['item']['price'], 0, -5),
            'description'       => $shopeeCall['item']['description'],
            'condition'         => '',
            'meetup'            => '',
            'delivery'          => '',
            'keyword'           => $shopeeCall['item']['categories'][0]['display_name'],
            'source'            => 'shopee'
        ];

        return $newshopee_item;
    }

    public function doShopeeSearch($page, $search) {
        $function = new Functions();
        $url = $this->shopeesearch_url.'?by=relevancy&keyword='.$search.'&limit=10&newest='.($page - 1) * 10 .'&order=desc&page_type=search&version=2';
        
        $client = new Client();

        try {                    
            $response = $client->request('GET', $url,['headers' => [
                'Referer' => 'https://shopee.ph/search?keyword='.$search,
                'Host' => 'shopee.ph',
                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:72.0) Gecko/20100101',
                'Accept' =>  '*/*',
                'Accept-Language' =>  'en-US,en;q=0.5',
                'Accept-Encoding' =>  'gzip, deflate, br',
                'X-Requested-With' =>  'XMLHttpRequest',
                'X-API-SOURCE' =>  'pc',
                'If-None-Match-' =>  '55b03-a042ede2409af7c9f5cec611617cc830',
                'Connection' =>  'keep-alive',
                'Cookie'    => 'SPC_IA=-1; SPC_EC="quRK+dbmAmoYx6GlBd1Xm9pmaXGgv8j3T5EhB0sxqg2kbf/TeiIjykoZgLJ4EyceSdHiCSND5oLZlBBo0YK7DvF+BPwnvoK4A/rMyiNtFF10NPEHwFCSQbC0AkLAWcFHyi5gfFmRs71abJrxTy+6njgaJujYNARD4S+sGbetS7Y="; SPC_F=AiulDFTVbbxYwtIbrxAOpcYl8ftrFrgE; REC_T_ID=f900ecb6-af72-11e9-bcd2-525400041f30; SPC_T_ID="et8TpbLi9Kii9tto7PGnS4syEnow+byS1vdhIb/2vJoeeBD60LlmM3BkUiI0CHVeGM9cwKwJa61GPRrZchCB5GHailUcVDeHYvlbbf8yIcQ="; SPC_U=144821993; SPC_T_IV="saeWthK/KxeioZC+Od1NzA=="; _ga=GA1.2.530237402.1564124398; cto_lwid=5e4f38db-c3d0-4809-b2e5-57f1bac435f4; _fbp=fb.1.1568792513387.1106700566; cto_bundle=Sk-Q018zVGY3Y3ZjRnEwMzFyeFZvN1ZxVWdxa1h4NEJUc3l4N2l0T1J1ZkMwSDVXSmxoUEFHNFBjTUFjM1RBTlM5bG1MT1klMkZ2bk5JbzhrZHNJWTlkdnVWWDZlJTJCRUwxNjYyQXp2Z1dJNXBVQ3ZnZnQ5dzV0aXNwWXc3NHR3bkRvdGFTVTNrbE1HV2xrM3BYRnlPZzNzWlQyeDV4Z0g1dlgyaXlTdDB5SFZRWEpEQ05NJTNE; _gcl_au=1.1.1335155595.1572500998; _gcl_aw=GCL.1579144543.Cj0KCQiAjfvwBRCkARIsAIqSWlPujAjAOXlCkTBnlV0YX7BwVCdpRHCvqeBQ6cuVV9nPkSnKd4jDQuQaAiciEALw_wcB; _med=refer; _gac_UA-61918643-6=1.1579144548.Cj0KCQiAjfvwBRCkARIsAIqSWlPujAjAOXlCkTBnlV0YX7BwVCdpRHCvqeBQ6cuVV9nPkSnKd4jDQuQaAiciEALw_wcB; G_ENABLED_IDPS=google; fbm_437897333028638=base_domain=.shopee.ph; CTOKEN=x1A9QDwaEeq7lsy7%2Fvdgww%3D%3D; SPC_SI=xojiuj9e37klj0bp9xk9z7l4sxl1mj37; _gid=GA1.2.1849928557.1579500877; csrftoken=XydwSX6uJ7ylqDvQTe2PjDdqgHx8q1Rw; welcomePkgShown=true; AMP_TOKEN=%24NOT_FOUND'
                ]], ['http_errors' => false]);
            
            $body = json_decode($response->getBody(), true);
            if($body['items'] != null) {
                $shopee = $this->createCarousellData($body , 'search');
                return $shopee;
            } else {
                return false;
            }
            
        }
        catch (\GuzzleHttp\Exception\ClientException $e) {
            return false;
        }
        catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return false;
        }
        catch (\GuzzleHttp\Exception\ConnectException $e) {
            return false;
        }
        catch (\GuzzleHttp\Exception\ServerException $e) {
            return false;
        }
        catch (\Exception $e) {
            return false;
        }
        if($searchShopee == false) {
            return false;
        }

       
    }

    public function getSimilarItem($catid, $id, $shopid, $page) {        
        $function = new Functions();
        $url = $this->shopeesimilaritem_url.'catid='.$catid.'&itemid='.$id.'&limit=10'.'&offset='.($page - 1) * 10 .'&recommend_type=27&shopid='.$shopid;
        
        $similarItem = $function->guzzleHttpCall($url);

        if($similarItem == false) {
            return false;
        }
        
        if(count($similarItem['data']['items']) > 0) {
            $shopee = $this->createCarousellData($similarItem);
            return $shopee['data'];
        } else {
            return false;
        }
    }

    private function createCarousellData($resultdata, $type = null) {
        // var_dump($resultdata);
        $shopeejsonfeed = [];        
        $count=0;

        foreach(($type == null ? $resultdata['data']['items'] : $resultdata['items']) as $newShopee) {
            $snippet = [];

            $snippet = [
                $newShopee['name'],
                'PHP '.number_format(substr($newShopee['price'], 0, -5), 0, '.', ','),
            ];

            $shopeejsonfeed[] = [
                'no'                =>  $count,
                'id'                =>  $newShopee['itemid'],
                'shopid'            =>  $newShopee['shopid'],
                'title'             =>  $newShopee['name'],
                'snippet'           =>  $snippet,
                'link'              =>  'https://shopee.ph/'.$this->treatTitle($newShopee['name']).'-i.'.$newShopee['shopid'].'.'.$newShopee['itemid'],
                'image'             =>  $this->shopeeimg_url.$newShopee['image'],
                'thumbnailimage'    =>  $this->shopeeimg_url.$newShopee['image'].'_tn',
                'shopid'            =>  $newShopee['shopid'],
                'source'            =>  'shopee'
            ];
            $count++;
        }
        return array(
            'data'      => $shopeejsonfeed,
            'total'     => $type == null ?  $resultdata['data']['total'] : $resultdata['total_count'],
            'result'    => true
        );
    }

    private function treatTitle($title) {
        if($title == null) {
            return $title;
        }
        // then replace blank space with -
        $treatTitle = str_replace(' ', '-', $title);
        $treatTitle = preg_replace('/[^A-Za-z0-9\-]/', '', $treatTitle);
        // Replaces multiple hyphens with single one.
        $treatTitle = preg_replace('/-+/', '-', $treatTitle);

        return $treatTitle;
    }
}
