<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Exception;
use App\User;
use App\Product;
use App\Ads;
use App\ProductPhoto;
use App\Admin;
use App\Http\Controllers\Functions;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Support\Facades\File;
use Jdenticon\Identicon;
use Tymon\JWTAuth\Contracts\JWTSubject;

class AdminController extends Controller
{
    public $admin;
    protected $jwt;
    protected $function;
    public $baseURL;

    /**
     * __contruct()
     *
     * @param Functions $function
     * @param JWTAuth $jwt
     * @param Admin $admin
     */
    public function __construct(JWTAuth $jwt, Admin $admin, Functions $function) {
      $this->admin    = $admin;
      $this->jwt      = $jwt;
      $this->function = $function;
      $this->baseURL  = Functions::getAppURL();
  }

  /**
     * method to check input param email and password
     * and also check if email exist on database
     *
     * @param $requestall
     * @return Array
     */
    protected function checkAll($requestall) {
      // check all $request
      $checkall = $this->admin->validateregisterUser($requestall);

      // check if email exist
      $checkemail = $this->admin->isemailExist($requestall['email']);

      if(!$checkall) {
          return array(
              'message'   =>  'input parameter is incorrect!',
              'result'    =>  false
          );
      } elseif($checkemail) {
          return array(
              'message'   =>  'email already exist!',
              'result'    =>  false
          );
      } else {
          return array(
              'message'   =>  '',
              'result'    =>  true
          );
      }
  }

  /**
   * method to register admin user
   *
   * @param $request
   * @return JSON
   */
  public function registerAdmin(Request $request) {
      // check all $request
      $checkall = $this->checkAll($request->all());

      if(!$checkall['result']) {
          return response()->json([
              'message'   =>  $checkall['message'],
              'result'    =>  $checkall['result']
          ]);
      }

      // create admin temp profile image
      $identicon = $this->createIdenticon($request->email);

      $this->admin->email         = $request->email;
      $this->admin->password      = $this->function->hash($request->password);
      $this->admin->profile_photo = $identicon['folder'].$identicon['filename'];

      if($this->admin->save()) {
          file_put_contents($identicon['folder'].$identicon['filename'], $identicon['identicon']);
          return response()->json([
              'message'   =>  '',
              'result'    =>  true
          ]);
      } else {
          return response()->json([
              'message'   =>  'Failed to save admin data!',
              'result'    =>  false
          ]);
      }
  }

  /**
   * method to update admin user profile_photo
   *
   * @param $request
   * @return JSON
   */
  public function updateAdminPhoto(Request $request) {
      // check if email exist
      $checkemail = $this->admin->isemailExist($request->email);

      if(!$checkemail) {
          return response()->json([
              'message'   =>  'User not found!',
              'result'    =>  false
          ]);
      }

      if(!$request->hasFile('profile_photo')){
          return response()->json([
              'message'   =>  'No image found!',
              'result'    =>  false
          ]);
      }

      // get the admin user profile folder
      $old_path = $this->admin->getAdminFolder($request->email);

      // get admin user data. we will use it later to delete eist photo
      $currentadmin = $this->admin->getAdminData($request->email);

      if(!$old_path) {
          // means, the system didnt create Identicon
          // so we will create one
          $old_path = $this->admin->createAdminFolder($request->email);
      }

      $profilephoto = $request->file('profile_photo');
      $name = md5($profilephoto->getClientOriginalName());
      $newprofilephoto = $name.'.'.$profilephoto->getClientOriginalExtension();
      $profile_photo  = $old_path.$newprofilephoto;

      $updatephoto = $this->admin::where('idadmin', $currentadmin['idadmin']);

      if($updatephoto->update([
          'profile_photo' => $profile_photo
          ])) {

          //move the image to its location
          $profilephoto->move($old_path,$newprofilephoto);

          // delete old photo
          unlink($currentadmin['profile_photo']);

          return response()->json([
              'message'   => '',
              'result'    => true
          ]);
      } else {
          return response()->json([
              'message'   => 'Failed to update!',
              'result'    => false
          ]);
      }
  }

  /**
   * method to update admin password
   *
   * @param $request
   * @return JSOn
   */
  public function updatePassword(Request $request) {
     // check if email exist
     $usertochange = $this->admin->getAdminData($request->email);
     if($usertochange == null) {
         return response()->json([
             'message'   =>  'User not found!',
             'result'    =>  false
         ]);
     }

      $old_password = $request->oldpassword;
      $new_password = $request->newpassword;

      // check old password to the database
      if(!password_verify($old_password, $usertochange['password'])) {
          return response()->json([
              'message'   => 'Password is not same as store in database!',
              'result'    => false
          ]);
      }

      // ok lets change that password
      $changepass = $this->admin::where('idadmin', $usertochange['idadmin']);

      $changepass->update([
          'password'  =>  $this->function->hash($new_password)
      ]);

      return response()->json([
          'message'   => '',
          'result'    => true
      ]);

  }

  /**
   * method to login user
   *
   * @param Request $request
   * @return Mix
   */
  public function loginAdmin(Request $request) {

      // check if email exist
      $emailexist = $this->admin->isemailExist($request->email);
      if(!$emailexist) {
          return response()->json([
              'message' => 'email dont exist!',
              'result'=> false
          ]);
      }

      // get admin object data
      $logadmin = $this->admin->getAdminData($request->email);

      // check password
      $checkemail = $this->function->verifyPassword($request->password, $logadmin->password);
      if(!$checkemail) {
        return response()->json([
          'message' => 'username, password not correct',
          'result'=> false
        ]);
      }

      try {

          if (!$token = $this->jwt->fromUser($logadmin)){
              return response()->json([
                  'message' => 'username, password not correct',
                  'result'=> false
              ]);
          }

      } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

          return response()->json([
              'message'=>'token_invalid',
              'result'=>false
          ]);
      } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

          return response()->json([
              'message' => $e->getMessage(),
              'result'=>false
          ]);
      }

      return response()->json([
          'token' => $token,
          'result'  => true
      ]);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh(){
      try {
      $token = Auth::refresh();
      } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json([
              'message'=>'token_invalid',
              'result'=>false
          ], 401);
      } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json([
              'message' => $e->getMessage(),
              'result'=>false
          ], 500);
      }

      return response()->json([
          'message'   => '',
          'result'    => true,
          'token'     => $token
      ]);
  }

  /**
   * Log the user out (Invalidate the token).
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function logoutAdmin() {

      try {
          Auth::logout(true);
      } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json([
              'message'=>'token_invalid',
              'result'=>false
          ], 401);
      } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json([
              'message' => $e->getMessage(),
              'result'=>false
          ], 500);
      }

      return response()->json([
          'message'   => '',
          'result'    => true
      ]);
  }

  /**
   * method to get Admin data using JWT token
   *
   * @param $request
   * @return JSON
   */
  public function getAdminData(Request $request) {
      $header = $request->header('Authorization');

      try {
          if(! $decoded = Auth::getPayload($header)->toArray()){
              return response()->json([
                  'message'   => 'access denied',
                  'result'    => false
              ]);
          }
      } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
          return response()->json([
              'message'   => 'Invalid Token',
              'result'    => false
          ]);
      } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
          return response()->json([
            'message' => 'Expired Token',
            'result'=>false
        ], 401);
      } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
          return response()->json([
              'message' => $e->getMessage(),
              'result'=>false
          ], 500);
      }

      $thisuser = $this->admin::where('idadmin', $decoded['sub'])->first();

      if($thisuser != null) {
          return response()->json([
              'data'      => [
                  'idadmin'       => $decoded['sub'],
                  'email'         => $thisuser['email'],
                  'profile_photo' => $this->baseURL.$thisuser['profile_photo'],
              ],
              'result'    => true
          ]);
      } else {
          return response()->json([
              'data'      => 'failed to get user info',
              'result'    => false
          ]);
      }
  }

  /**
   * method to create Identicon
   *
   * @param $email
   * @return array
   */
  protected function createIdenticon($email) {
      // lets create Identicon

      // lets create time for name purpose
      $name = time();

      $img = new \Jdenticon\Identicon();
      $img->setValue($email);
      $img->setSize(150);

      // lets create admin folder
      $adminfolder = $this->admin->createAdminFolder($email);

      /**
       * we need to Turn on output buffering coz there's no way we can get the image
       * then Clean (erase) the output buffer and turn off output buffering
       */
      ob_start();
          $photo  = $img->displayImage('png');
          $binary = $img->getImageData('png');
          $identicon = ob_get_contents();
      ob_end_clean();

      // name of the temporary profile image
      $filename = $adminfolder[1].'_'.$name.'.png';

      return array(
          'folder'    => $adminfolder[0],
          'filename'  => $filename,
          'identicon' => $identicon
      );
  }

  /**
   * Get dashboard
   *
   * @return JsonResponse
   */
  public function getDashboard()
  {
    return response()->json([
      'data' => [
        'usersCount' => User::count(),
        'productsCount' => Product::count(),
        'adsCount' => Ads::count()
      ],
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Get products
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getProducts(Request $request)
  {
    $user = new User();
    $page = $request->has('page') ? $request->page : 1;
    $per_page = 10;
    $data = [];
    $query = Product::join('users', 'products.iduser', '=', 'users.iduser')
      ->skip(($page * $per_page) - $per_page)
      ->take($per_page)
      ->get();

    foreach ($query as $q) {
      $photos = [];
      $primary_photo = '';
      $product_photos = ProductPhoto::where('idproduct', $q['idproduct'])
        ->get();

      foreach ($product_photos as $product_photo) {
        $photos[] = env('APP_URL') . $product_photo['image'];

        if ($product_photo['primary']) {
          $primary_photo = env('APP_URL') . $product_photo['image'];
        }
      }

      $data[] = [
        'id' => $q['idproduct'],
        'title' => $q['title'],
        'price' => 'PHP ' . number_format($q['price'], 0, '.', ','),
        'status' => $q['condition'],
        'seller' => [
          'id' => $q['iduser'],
          'profilePhoto' => $user->profilePath($q['profile_photo']),
          'username' => $q['username'],
        ],
        'photos' => $photos,
        'primaryPhoto' => $primary_photo
      ];
    }

    return response()->json([
      'data' => $data,
      'total' => Product::count(),
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Search products
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function searchProducts(Request $request)
  {
    $user = new User();
    $page = $request->has('page') ? $request->page : 1;
    $per_page = 10;
    $data = [];
    $product_where = Product::where('title', 'LIKE', "%{$request->key}%");
    $product_count = $product_where ->count();
    $query = $product_where
      ->join('users', 'products.iduser', '=', 'users.iduser')
      ->skip(($page * $per_page) - $per_page)
      ->take($per_page)
      ->get();

    foreach ($query as $q) {
      $photos = [];
      $primary_photo = '';
      $product_photos = ProductPhoto::where('idproduct', $q['idproduct'])
        ->get();

      foreach ($product_photos as $product_photo) {
        $photos[] = env('APP_URL') . $product_photo['image'];

        if ($product_photo['primary']) {
          $primary_photo = env('APP_URL') . $product_photo['image'];
        }
      }

      $data[] = [
        'id' => $q['idproduct'],
        'title' => $q['title'],
        'price' => 'PHP ' . number_format($q['price'], 0, '.', ','),
        'status' => $q['status'],
        'seller' => [
          'id' => $q['iduser'],
          'profilePhoto' => $user->profilePath($q['profile_photo']),
          'username' => $q['username'],
        ],
        'photos' => $photos,
        'primaryPhoto' => $primary_photo
      ];
    }

    return response()->json([
      'data' => $data,
      'total' => $product_count,
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Get product
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getProduct(Request $request)
  {
    if (!$request->has('id')) {
      return response()->json([
        'data' => [],
        'result' => false,
        'message' => 'PARAMETER ID IS MISSING'
      ]);
    }

    $query = Product::where('idproduct', $request->id)
      ->join('users', 'products.iduser', '=', 'users.iduser')
      ->first();

    if (!$query) {
      return response()->json([
        'data' => [],
        'result' => false,
        'message' => 'PRODUCT NOT FOUND'
      ]);
    }

    $user = new User();
    $photos = [];
    $primary_photo = '';
    $product_photos = ProductPhoto::where('idproduct', $query['idproduct'])
      ->get();

    foreach ($product_photos as $product_photo) {
      $photos[] = env('APP_URL') . $product_photo['image'];

      if ($product_photo['primary']) {
        $primary_photo = env('APP_URL') . $product_photo['image'];
      }
    }

    $data = [
      'id' => $query['idproduct'],
      'title' => $query['title'],
      'description' => $query['description'],
      'price' => 'PHP ' . number_format($query['price'], 0, '.', ','),
      'category' => [
        'id' => $query['categoryid'],
        'name' => array_search($query['categoryid'], config('corousell_category'))
      ],
      'status' => $query['status'],
      'condition' => $query['condition'],
      'meetup' => $query['meetup'],
      'delivery' => $query['delivery'],
      'seller' => [
        'id' => $query['iduser'],
        'profilePhoto' => $user->profilePath($query['profile_photo']),
        'username' => $query['username'],
      ],
      'photos' => $photos,
      'primary_photo' => $primary_photo
    ];

    return response()->json([
      'data' => $data,
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Get users
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getUsers(Request $request)
  {
    $user = new User();
    $page = $request->has('page') ? $request->page : 1;
    $per_page = 10;
    $data = [];
    $query = $user->skip(($page * $per_page) - $per_page)
      ->take($per_page)
      ->get();

    foreach ($query as $q) {
      $data[] = [
        'id' => $q['iduser'],
        'firstName' => $q['firstname'],
        'lastName' => $q['lastname'],
        'email' => $q['email'],
        'username' => $q['username'],
        'contactNumber' => $q['contactno'],
        'isEmailVerified' => $q['emailverify'],
        'profilePhoto' => $user->profilePath($q['profile_photo'])
      ];
    }

    return response()->json([
      'data' => $data,
      'total' => User::count(),
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Search users
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function searchUsers(Request $request)
  {
    $user = new User();
    $page = $request->has('page') ? $request->page : 1;
    $per_page = 10;
    $data = [];
    $user_where = $user->where('email', 'LIKE', "%{$request->key}%")
      ->orWhere('firstname', 'LIKE', "%{$request->key}%")
      ->orWhere('lastname', 'LIKE', "%{$request->key}%")
      ->orWhere('username', 'LIKE', "%{$request->key}%")
      ->orWhere('contactno', 'LIKE', "%{$request->key}%");
    $user_count = $user_where->count();

    $query = $user_where->skip(($page * $per_page) - $per_page)
      ->take($per_page)
      ->get();

    foreach ($query as $q) {
      $data[] = [
        'id' => $q['iduser'],
        'firstName' => $q['firstname'],
        'lastName' => $q['lastname'],
        'email' => $q['email'],
        'username' => $q['username'],
        'contactNumber' => $q['contactno'],
        'isEmailVerified' => $q['emailverify'],
        'profilePhoto' => $user->profilePath($q['profile_photo'])
      ];
    }

    return response()->json([
      'data' => $data,
      'total' => $user_count,
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Get user
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getUser(Request $request)
  {
    if (!$request->has('id')) {
      return response()->json([
        'data' => [],
        'result' => false,
        'message' => 'PARAMETER ID IS MISSING'
      ]);
    }

    $user = new User();
    $query = $user::where('iduser', $request->id)
      ->first();

    if (!$query) {
      return response()->json([
        'data' => [],
        'result' => false,
        'message' => 'USER NOT FOUND'
      ]);
    }

    $data = [
      'id' => $query['iduser'],
      'firstName' => $query['firstname'],
      'lastName' => $query['lastname'],
      'email' => $query['email'],
      'username' => $query['username'],
      'contactNumber' => $query['contactno'],
      'isEmailVerified' => $query['emailverify'],
      'profilePhoto' => $user->profilePath($query['profile_photo'])
    ];

    return response()->json([
      'data' => $data,
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Get ads
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getAds(Request $request)
  {
    $page = $request->has('page') ? $request->page : 1;
    $per_page = 10;
    $data = [];
    $query = Ads::skip(($page * $per_page) - $per_page)
      ->take($per_page)
      ->orderBy('idads', 'desc')
      ->get();

    foreach ($query as $q) {
      $data[] = [
        'id' => $q['idads'],
        'title' => $q['adstitle'],
        'space' => $q['adsspaces'],
        'link' => $q['adslink'],
        'start' => date("Y-m-d", strtotime($q['adsstart'])),
        'end' => date("Y-m-d", strtotime($q['adsend'])),
        'image' => env('APP_URL') . $q['adsimage']
      ];
    }

    return response()->json([
      'data' => $data,
      'total' => Ads::count(),
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Search ads
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function searchAds(Request $request)
  {
    $page = $request->has('page') ? $request->page : 1;
    $per_page = 10;
    $data = [];
    $ad_where = Ads::where('adstitle', 'LIKE', "%{$request->key}%");
    $ad_count =$ad_where->count();
    $query = $ad_where->skip(($page * $per_page) - $per_page)
      ->take($per_page)
      ->get();

    foreach ($query as $q) {
      $data[] = [
        'id' => $q['idads'],
        'title' => $q['adstitle'],
        'space' => $q['adsspaces'],
        'link' => $q['adslink'],
        'start' => date("Y-m-d", strtotime($q['adsstart'])),
        'end' => date("Y-m-d", strtotime($q['adsend'])),
        'image' => env('APP_URL') . $q['adsimage']
      ];
    }

    return response()->json([
      'data' => $data,
      'total' => $ad_count,
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Get ad
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function getAd(Request $request)
  {
    if (!$request->has('id')) {
      return response()->json([
        'data' => [],
        'result' => false,
        'message' => 'REQUIRED PARAMETER: id'
      ]);
    }

    $query = Ads::where('idads', $request->id)->first();

    if (!$query) {
      return response()->json([
        'data' => [],
        'result' => false,
        'message' => 'AD NOT FOUND'
      ]);
    }

    $data = [
      'id' => $query['idads'],
      'title' => $query['adstitle'],
      'space' => $query['adsspaces'],
      'link' => $query['adslink'],
      'start' => $query['adsstart'],
      'end' => $query['adsend'],
      'image' => env('APP_URL') . $query['adsimage']
    ];

    return response()->json([
      'data' => $data,
      'result' => true,
      'message' => 'SUCCESS'
    ]);
  }

  /**
   * Create ad
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function createAd(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'title' => 'required',
      'image' => 'required',
      'spaces' => 'required',
      'link' => 'required',
      'start' => 'required',
      'end' => 'required',
    ]);

    if ($validate->fails()) {
      return response()->json([
        'message'   => 'REQUIRED PARAMETERS: title, image, spaces, link, start, end',
        'result'    => false
      ]);
    }

    DB::beginTransaction();

    try {
      $image = $request->file('image');
      $dir = 'img/advertisement/';
      $new_image_name = md5($image->getClientOriginalName())
        . time()
        . '.'
        . $image->getClientOriginalExtension();

      $ad = new Ads();
      $ad->adstitle    = $request->title;
      $ad->adsimage    = $dir . $new_image_name;
      $ad->adsspaces   = $request->spaces;
      $ad->adslink     = $request->link;
      $ad->adsstart    = $request->start;
      $ad->adsend      = $request->end;

      if ($ad->save()) {
        $image->move($dir, $new_image_name);
      }

      DB::commit();

      return response()->json([
        'message'   => 'SUCCESS',
        'result'    => true
      ]);

    } catch (Exception $e) {
      DB::rollback();

      return response()->json([
        'message'   => 'FAILED TO CREATE AD',
        'result'    => false
      ]);
    }
  }

  /**
   * Update ad
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function updateAd(Request $request)
  {
    $validate = Validator::make($request->all(), [
      'id' => 'required',
      'title' => 'required',
      'spaces' => 'required',
      'link' => 'required',
      'start' => 'required',
      'end' => 'required',
    ]);

    if ($validate->fails()) {
      return response()->json([
        'message'   => 'REQUIRED PARAMETERS: id, title, spaces, link, start, end',
        'result'    => false
      ]);
    }

    DB::beginTransaction();

    try {
      $ad = Ads::where('idads', $request->id);
      $ad_data = $ad->first();

      if (!$ad_data) {
        return response()->json([
          'message'   => 'AD NOT FOUND',
          'result'    => false
        ]);
      }

      $data = [];
      $dir = 'img/advertisement/';
      $old_image_path = app()->basePath('public/' . $ad_data['adsimage']);
      $new_image_name = '';
      $image = $request->file('image');
      $has_image = $request->hasFile('image');

      if ($has_image){
        $new_image_name = md5($image->getClientOriginalName())
          . time()
          . '.'
          . $image->getClientOriginalExtension();
        $data['adsimage'] = $dir . $new_image_name;
      }

      $data['adstitle']    = $request->title;
      $data['adsspaces']   = $request->spaces;
      $data['adslink']     = $request->link;
      $data['adsstart']    = $request->start;
      $data['adsend']      = $request->end;

      if ($ad->update($data) && $has_image) {
        $image->move($dir, $new_image_name);

        if(file_exists($old_image_path)) unlink($old_image_path);
      }

      DB::commit();

      return response()->json([
        'message'   => 'SUCCESS',
        'result'    => true
      ]);

    } catch (Exception $e) {
      DB::rollback();

      return response()->json([
        'message'   => 'FAILED TO UPDATE AD',
        'result'    => false
      ]);
    }
  }

  /**
   * Delete ad
   *
   * @param Request $request
   * @return JsonResponse
   */
  public function deleteAd(Request $request) {
    if (!$request->has('id')) {
      return response()->json([
        'message' => 'REQUIRED PARAMETER: id',
        'result' => false
      ]);
    }

    DB::beginTransaction();

    try {
      $ad = Ads::where('idads', $request->id);
      $ad_data = $ad->first();
      $image_path = app()->basePath('public/' . $ad_data['adsimage']);

      if (!$ad_data) {
        return response()->json([
          'message'   => 'AD NOT FOUND',
          'result'    => false
        ]);
      }
      if ($ad->delete() && file_exists($image_path)) {
        unlink($image_path);
      }

      DB::commit();
    } catch (Exception $e) {
      DB::rollback();

      return response()->json([
        'error' => $e->getMessage(),
        'message'   => 'FAILED TO DELETE AD',
        'result'    => false
      ]);
    }

    return response()->json([
      'message' => 'SUCCESS',
      'result' => true
    ]);
  }
}
