<?php

namespace App\Http\Controllers;

use ccxt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use App\Binance;

class ExchangeController extends Controller
{
  /**
  * Get settings
  *
  * @param Request $request
  * @return void
  */
  public function getSettings(Request $request)
  {
    try {
      if (!$auth = Auth::getPayload($request->header('Authorization'))) {
        return response()->json([
          'data' => [],
          'message'   => 'Access denied',
          'result'    => false
        ]);
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json([
        'data' => [],
        'message'   => 'Invalid Token',
        'result'    => false
      ]);
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Expired Token',
        'result' => false
      ], 401);
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Authentication error',
        'result' => false
      ], 500);
    }

    if (!$request->has('name')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: name',
        'result' => false
      ]);
    }

    if (!$request->has('pair')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: pair',
        'result' => false
      ]);
    }

    $exchange_name = ucfirst($request->name);
    $exchange = "App\\{$exchange_name}";

    if (!class_exists($exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange not found',
        'result' => false
      ]);
    }

    $user_exchange = $exchange::where('user_id', $auth['sub'])
      ->first();

    if (!$user_exchange) {
      return response()->json([
        'data' => [],
        'message' => 'User exchange not found',
        'result' => false
      ]);
    }


    if (!$user_exchange->pairs) {
      return response()->json([
        'data' => [],
        'message' => 'No pair settings found',
        'result' => false
      ]);
    }

    if (!isset($user_exchange->pairs[$request->pair])) {
      return response()->json([
        'data' => [],
        'message' => 'Pair setting not found',
        'result' => false
      ]);
    }

    return response()->json([
      'data' => $user_exchange->pairs[$request->pair],
      'message' => 'Success',
      'result' => true
    ]);
  }

  /**
  * Save settings
  *
  * @param Request $request
  * @return void
  */
  public function saveSettings(Request $request)
  {
    try {
      if (!$auth = Auth::getPayload($request->header('Authorization'))) {
        return response()->json([
          'data' => [],
          'message'   => 'Access denied',
          'result'    => false
        ]);
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json([
        'data' => [],
        'message'   => 'Invalid Token',
        'result'    => false
      ]);
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Expired Token',
        'result' => false
      ], 401);
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Authentication error',
        'result' => false
      ], 500);
    }

    if (!$request->has('name')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: name',
        'result' => false
      ]);
    }

    if (!$request->has('pair')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: pair',
        'result' => false
      ]);
    }

    if (!$request->has('buy') && !$request->has('sell')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: buy or sell',
        'result' => false
      ]);
    }

    $exchange_name = ucfirst($request->name);
    $exchange = "App\\{$exchange_name}";

    if (!class_exists($exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange not found',
        'result' => false
      ]);
    }

    $user_exchange = $exchange::where('user_id', $auth['sub'])
      ->first();

    if (!$user_exchange) {
      $user_exchange = new $exchange();
      $user_exchange->user_id = $auth['sub'];
    }

    if ($request->has('buy')) {
      $order = ['buy' => $request->buy];

      if ($user_exchange->pairs) {
        if (isset($user_exchange->pairs[$request->pair])) {
          $pair = (object) array_merge(
            (array) $user_exchange->pairs[$request->pair], (array) $order
          );
          $user_exchange->pairs = (object) array_merge(
            (array) $user_exchange->pairs, (array) [$request->pair => $pair]
          );
        } else {
          $user_exchange->pairs = (object) array_merge(
            (array) $user_exchange->pairs, (array) [$request->pair => $order]
          );
        }
      } else {
        $user_exchange->pairs =  [
          $request->pair => $order
        ];
      }
    }

    if ($request->has('sell')) {
      $order = ['sell' => $request->sell];

      if ($user_exchange->pairs) {
        if (isset($user_exchange->pairs[$request->pair])) {
          $pair = (object) array_merge(
            (array) $user_exchange->pairs[$request->pair], (array) $order
          );
          $user_exchange->pairs = (object) array_merge(
            (array) $user_exchange->pairs, (array) [$request->pair => $pair]
          );
        } else {
          $user_exchange->pairs = (object) array_merge(
            (array) $user_exchange->pairs, (array) [$request->pair => $order]
          );
        }
      } else {
        $user_exchange->pairs =  [
          $request->pair => $order
        ];
      }
    }

    if (!$user_exchange->save()) {
      return response()->json([
        'data' => [],
        'message' => 'Failed to save settings',
        'result' => false
      ]);
    }

    return response()->json([
      'data' => [],
      'message' => 'Success',
      'result' => true
    ]);
  }

  /**
  * Get order history
  *
  * @param Request $request
  * @return void
  */
  public function getOrderHistory(Request $request)
  {
    try {
      if (!$auth = Auth::getPayload($request->header('Authorization'))) {
        return response()->json([
          'data' => [],
          'message'   => 'Access denied',
          'result'    => false
        ]);
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json([
        'data' => [],
        'message'   => 'Invalid Token',
        'result'    => false
      ]);
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Expired Token',
        'result' => false
      ], 401);
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Authentication error',
        'result' => false
      ], 500);
    }

    if (!$request->has('name')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: name',
        'result' => false
      ]);
    }

    if (!$request->has('pair')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: pair',
        'result' => false
      ]);
    }

    $ccxt_exchange = "\ccxt\\{$request->name}";

    if (!class_exists($ccxt_exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange is not supported',
        'result' => false
      ]);
    }

    $exchange_name = ucfirst($request->name);
    $exchange = "App\\{$exchange_name}";

    if (!class_exists($exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange not found',
        'result' => false
      ]);
    }

    $user_exchange = $exchange::where('user_id', $auth['sub'])
      ->first();

    if (!$user_exchange) {
      return response()->json([
        'data' => [],
        'message' => 'User exchange not found',
        'result' => false
      ]);
    }

    if (!$user_exchange->apiKey && !$user_exchange->secretKey) {
      return response()->json([
        'data' => [],
        'message' => 'No exchange credential found',
        'result' => false
      ]);
    }

    $exchange = new $ccxt_exchange(array(
      'apiKey' => Crypt::decrypt($user_exchange->apiKey),
      'secret' => Crypt::decrypt($user_exchange->secretKey)
    ));
    $orderHistory = $exchange->fetch_orders($request->pair);

    if (!$orderHistory) {
      return response()->json([
        'data' => [],
        'message' => 'No order history',
        'result' => true
      ]);
    }

    return response()->json([
      'data' => $orderHistory,
      'message' => 'Success',
      'result' => true
    ]);
  }

  /**
  * Get open orders
  *
  * @param Request $request
  * @return void
  */
  public function getOpenOrders(Request $request)
  {
    try {
      if (!$auth = Auth::getPayload($request->header('Authorization'))) {
        return response()->json([
          'data' => [],
          'message'   => 'Access denied',
          'result'    => false
        ]);
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json([
        'data' => [],
        'message'   => 'Invalid Token',
        'result'    => false
      ]);
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Expired Token',
        'result' => false
      ], 401);
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Authentication error',
        'result' => false
      ], 500);
    }

    if (!$request->has('name')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: name',
        'result' => false
      ]);
    }

    if (!$request->has('pair')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: pair',
        'result' => false
      ]);
    }

    $ccxt_exchange = "\ccxt\\{$request->name}";

    if (!class_exists($ccxt_exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange is not supported',
        'result' => false
      ]);
    }

    $exchange_name = ucfirst($request->name);
    $exchange = "App\\{$exchange_name}";

    if (!class_exists($exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange not found',
        'result' => false
      ]);
    }

    $user_exchange = $exchange::where('user_id', $auth['sub'])
      ->first();

    if (!$user_exchange) {
      return response()->json([
        'data' => [],
        'message' => 'User exchange not found',
        'result' => false
      ]);
    }

    if (!$user_exchange->apiKey && !$user_exchange->secretKey) {
      return response()->json([
        'data' => [],
        'message' => 'No exchange credential found',
        'result' => false
      ]);
    }

    $exchange = new $ccxt_exchange(array(
      'apiKey' => Crypt::decrypt($user_exchange->apiKey),
      'secret' => Crypt::decrypt($user_exchange->secretKey)
    ));
    $openOrders = $exchange->fetch_open_orders($request->pair);

    if (!$openOrders) {
      return response()->json([
        'data' => [],
        'message' => 'No open orders',
        'result' => true
      ]);
    }

    return response()->json([
      'data' => $openOrders,
      'message' => 'Success',
      'result' => true
    ]);
  }

  /**
  * Get balance
  *
  * @param Request $request
  * @return void
  */
  public function getBalance(Request $request)
  {
    try {
      if (!$auth = Auth::getPayload($request->header('Authorization'))) {
        return response()->json([
          'data' => [],
          'message'   => 'Access denied',
          'result'    => false
        ]);
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json([
        'data' => [],
        'message'   => 'Invalid Token',
        'result'    => false
      ]);
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Expired Token',
        'result' => false
      ], 401);
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Authentication error',
        'result' => false
      ], 500);
    }

    if (!$request->has('name')) {
      return response()->json([
        'data' => [],
        'message' => 'Missing parameter: name',
        'result' => false
      ]);
    }

    $ccxt_exchange = "\ccxt\\{$request->name}";

    if (!class_exists($ccxt_exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange is not supported',
        'result' => false
      ]);
    }

    $exchange_name = ucfirst($request->name);
    $exchange = "App\\{$exchange_name}";

    if (!class_exists($exchange)) {
      return response()->json([
        'data' => [],
        'message' => 'Exchange not found',
        'result' => false
      ]);
    }

    $user_exchange = $exchange::where('user_id', $auth['sub'])
      ->first();

    if (!$user_exchange) {
      return response()->json([
        'data' => [],
        'message' => 'User exchange not found',
        'result' => false
      ]);
    }

    if (!$user_exchange->apiKey && !$user_exchange->secretKey) {
      return response()->json([
        'data' => [],
        'message' => 'No exchange credential found',
        'result' => false
      ]);
    }

    $exchange = new $ccxt_exchange(array(
      'apiKey' => Crypt::decrypt($user_exchange->apiKey),
      'secret' => Crypt::decrypt($user_exchange->secretKey)
    ));
    $balance = $exchange->fetch_balance();

    if (!$balance) {
      return response()->json([
        'data' => [],
        'message' => 'Failed to load balance',
        'result' => false
      ]);
    }

    unset($balance['info']);

    return response()->json([
      'data' => $balance,
      'message' => 'Success',
      'result' => true
    ]);
  }

  public function getBinanceListenKey(Request $request) {
    try {
      if (!$auth = Auth::getPayload($request->header('Authorization'))) {
        return response()->json([
          'data' => [],
          'message'   => 'Access denied',
          'result'    => false
        ]);
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json([
        'data' => [],
        'message'   => 'Invalid Token',
        'result'    => false
      ]);
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Expired Token',
        'result' => false
      ], 401);
    } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json([
        'data' => [],
        'message' => 'Authentication error',
        'result' => false
      ], 500);
    }

    $binance = Binance::where('user_id', $auth['sub'])
    ->first();

    if (!$binance['apiKey']) {
      return response()->json([
        'data' => [],
        'message' => 'No api key found',
        'result' => false
      ]);
    }

    $url = 'https://api.binance.com/api/v3/userDataStream';
    $ch = curl_init($url);
    $api_key = Crypt::decrypt($binance['apiKey']);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ['X-MBX-APIKEY: ' . $api_key]);

    $response = json_decode(curl_exec($ch), true);
    curl_close($ch);

    if (!$response) {
      return response()->json([
        'data' => [],
        'message' => 'Binance api call error',
        'result' => false
      ]);
    }

    if (!isset($response['listenKey'])) {
      return response()->json([
        'data' => [],
        'message' => 'Binance authentication error',
        'result' => false
      ]);
    }

    return response()->json([
      'data' => [
        'listenKey' => $response['listenKey']
      ],
      'result' => true
    ]);

  }
}
