<?php

namespace App;

use Validator;
use Jenssegers\Mongodb\Eloquent\Model;

class Ads extends Model
{
    protected $table = 'ad';

    static function validateAdsCreate($request) {
        // lets validate
        $validator = Validator::make($request, [
            'adstitle'          => 'required',
            'adsimage'          => 'required',
            'adsspaces'         => 'required',
            'adslink'           => 'required',
            'adsstart'          => 'required',
            'adsend'            => 'required',
        ]);
        /**
         * me: if something went wrong on our validation then say something.
         * you: something.
         */
        if ($validator->fails()) {
            return false;
        }else{
            return true;
        }
    }
}
