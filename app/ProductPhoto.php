<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class ProductPhoto extends Model
{
    protected $table = 'product_photo';

    /**
     * method to get product photo data
     *
     * @param $idphoto
     * @return $productphoto
     */
    public function getProductPhotoData($idphoto) {
        $productphoto = ProductPhoto::find('idphoto', $idphoto);

        return $productphoto;
    }
}
